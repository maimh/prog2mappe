## ChaosGame
Mai Mathilde Hertaas <br/>
Vilde Enger Gravdal

IDATG-2003 Programmering 2 

## Project Description

The project has been developed to simulate the Chaos Game, a method of creating fractals using a simple algorithm. The application allows users to observe the formation of fractals through iterative point plotting. It includes functionalities for setting initial points, adjusting parameters, and visualizing the resulting fractal patterns. The system also provides options to save and load configurations, making it easier to explore different fractal structures and their properties.

## Running the application
The application can start  by using the below command:

mvn javafx:run

The tests can be runned by using:

mvn test

## Link to repository

https://gitlab.stud.idi.ntnu.no/maimh/prog2mappe

## Project structure
<pre>
src <br/>
└── main 
    └── java <br/>
        ├── backend <br/>
        │   └── chaosgame <br/>
        │       ├── ChaosCanvas <br/>
        │       ├── ChaosGame <br/>
        │       └── ChaosGameDescription <br/>
        │   └── filehandler <br/>
        │       └── ChaosGameFileHandler <br/>
        │   └── interfaces <br/>
        │       └── ChaosGameObserver <br/>
        │   └── math <br/>
        │       ├── Complex <br/>
        │       ├── Matrix2x2 <br/>
        │       └── Vector2D <br/>
        │   └── transformations <br/>
        │       ├── AffineTransform2D <br/>
        │       ├── JuliaTransform <br/>
        │       └── Transform2D <br/>
        └── frontend <br/>
            └── controllers <br/>
                ├── ButtonController <br/>
                └── DrawingController <br/>
            └── draw <br/>
                └── Drawing <br/>
            └── models <br/>
                ├── ButtonModel <br/>
                ├── DrawingModel <br/>
                └── FileInstance <br/>
            └── views <br/>
                ├── ButtonView <br/>
                ├── DrawingView <br/>
                ├── App <br/>
                ├── FilePicker <br/>
                └── Handler <br/>
    └── resources <br/>
        ├── css <br/>
        └── transformationfiles <br/>
└── test <br/>
    └── java <br/>
        ├── ChaosGameTest <br/>
        ├── ChaosCanvasTest <br/>
        └── DrawingControllerTest <br/>

<pre>












