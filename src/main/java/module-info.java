module frontend {
  requires javafx.controls;
  requires javafx.fxml;
  requires javafx.graphics;


  opens frontend to javafx.fxml;
  exports frontend;
}