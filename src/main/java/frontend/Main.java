package frontend;

import backend.chaosgame.ChaosCanvas;
import backend.chaosgame.ChaosGame;
import backend.chaosgame.ChaosGameDescription;
import backend.filehandler.ChaosGameFileHandler;
import frontend.draw.Drawing;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * The main class for the Chaos Game application.
 * Extends the Application class from JavaFX.

 */
public class Main extends Application {

  /**
   * The main method for the Chaos Game application.
   *
   * @param args the command-line arguments
   */
  public static void main(String[] args) {
    launch(args);
  }

  /**
   * Starts the Chaos Game application.
   *
   * @param primaryStage the primary stage for the application
   * @throws FileNotFoundException if the file is not found
   */
  @Override
  public void start(Stage primaryStage) throws FileNotFoundException {
    // Read data from file using FileReader class
    String filePath = "src/main/resources/transformationfiles/barnsley.txt";

    ChaosGameDescription fileHandler = ChaosGameFileHandler.readFromFile(filePath);

    int canvasWidth = 500; // Width of the canvas
    int canvasHeight = 500; // Height of the canvas

    ChaosCanvas canvas = new ChaosCanvas(canvasWidth, canvasHeight);
    ChaosGame game = new ChaosGame(canvas, fileHandler);

    // Run transformation steps
    int steps = 999999;
    game.runSteps(steps);

    // Create Canvas
    Canvas javafxCanvas = new Canvas(canvasWidth, canvasHeight);
    Pane canvasPane = new Pane(javafxCanvas);

    // Draw lines using data from file
    Drawing.drawTransformation(canvasPane, canvas);

    // Add Canvas to layout
    HBox root = new HBox(canvasPane);

    // Set up scene and show the stage
    Scene scene = new Scene(root);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Chaos Game");
    primaryStage.show();
  }
}