package frontend.controllers;

import backend.chaosgame.ChaosGame;
import backend.interfaces.ChaosGameObserver;
import frontend.models.ButtonModel;
import frontend.models.DrawingModel;
import frontend.views.DrawingView;
import javafx.scene.layout.HBox;

/**
 * Controller for handling drawing actions in the Chaos Game application. Implements the
 * ChaosGameObserver interface to receive updates from the ChaosGame.
 */
public class DrawingController implements ChaosGameObserver {

  private ChaosGame game;
  private DrawingView view;
  private DrawingModel model;

  /**
   * Constructs a DrawingController with a ChaosGame and a container for the view.
   *
   * @param game      the ChaosGame to control
   * @param container the HBox container for the view
   */
  public DrawingController(ChaosGame game, HBox container) {
    this.game = game;
    createView(container);
    game.regigsterObserver(this);
  }

  /**
   * Creates a DrawingView with a DrawingModel and adds it to the container.
   *
   * @param container the HBox container for the view
   */
  public void createView(HBox container) {
    model = mapToView();
    view = new DrawingView(model, container);
  }

  /**
   * Maps the ChaosGame to a DrawingModel.
   *
   * @return the DrawingModel
   */
  public DrawingModel mapToView() {
    DrawingModel model = new DrawingModel();
    model.data = game.getCanvas();
    model.canvasHeight = game.getCanvasHeight();
    model.canvasWidth = game.getCanvasWidth();
    return model;
  }

  /**
   * Updates the DrawingView with the latest data from the ChaosGame.
   */
  @Override
  public void update() {
    var model = mapToView();
    view.draw(model);
  }
}

