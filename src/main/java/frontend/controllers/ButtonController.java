package frontend.controllers;

import backend.chaosgame.ChaosCanvas;
import backend.chaosgame.ChaosGame;
import backend.chaosgame.ChaosGameDescription;
import backend.filehandler.ChaosGameFileHandler;
import backend.interfaces.ChaosGameObserver;
import backend.math.Complex;
import backend.transformations.AffineTransform2D;
import backend.transformations.JuliaTransform;
import frontend.App;
import frontend.Handler;
import frontend.draw.Drawing;
import frontend.models.ButtonModel;
import frontend.models.FileInstance;
import frontend.views.ButtonView;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;

/**
 * Controller for handling button actions in the Chaos Game application. Implements the
 * ChaosGameObserver interface to receive updates from the ChaosGame.
 */
public class ButtonController implements ChaosGameObserver {

  private ButtonView view;
  private ChaosGame chaosGame;

  /**
   * Constructs a ButtonController with a ChaosGame and a container for the view.
   *
   * @param chaosGame the ChaosGame to control
   * @param container the HBox container for the view
   */
  public ButtonController(ChaosGame chaosGame, HBox container) {
    this.chaosGame = chaosGame;
    chaosGame.regigsterObserver(this);
    createView(container);
  }

  /**
   * Creates a ButtonView with a ButtonModel and adds it to the container.
   *
   * @param container the HBox container for the view
   * @return the created ButtonView
   */
  public ButtonView createView(HBox container) {
    ButtonModel model = mapToView();

    view = new ButtonView(container, model);
    return view;
  }


  /**
   * Updates the view with new information from the ChaosGame.
   */
  @Override
  public void update() {
    ButtonModel model = mapToView();
    view.draw(model);
  }

  /**
   * Maps the ChaosGame to a ButtonModel.
   *
   * @return the ButtonModel mapped from the ChaosGame
   */
  public ButtonModel mapToView() {
    ButtonModel model = new ButtonModel();
    ChaosGame chaosGameRef = this.chaosGame;
    model.eventHandler = new Handler() {
      @Override
      public void handleFile(String path) {
        chaosGame.saveToFile(path);
      }

      @Override
      public void handleClear() {
        chaosGame.clear();
        chaosGame.triggerNotify();
      }

      @Override
      public void handleGetFile(String fileName) {
        chaosGameRef.readDescriptionFromFile(fileName);
      }

      @Override
      public void handleJuliaData(double xmin, double ymin, double xmax, double ymax,
          double imaginary, double real, int steps) {
        handleCreateNewJulia(xmin, ymin, xmax, ymax, imaginary, real, steps);
      }

      @Override
      public void handleAffineData(double xmin, double ymin, double xmax, double ymax,
          double[] transform1,
          double[] transform2, double[] transform3, int steps) {
        handleCreateNewAffline(xmin, ymin, xmax, ymax, transform1, transform2, transform3, steps);
      }

      @Override
      public void handleSaveFile(String fileName) {
        chaosGame.saveToFile(fileName);
      }
    };
    var fileList = getFiles();
    model.files = fileList;
    return model;
  }



  /**
   * Gets the list of saved Chao Game Description files.
   *
   * @return the list of FileInstances representing the saved files
   */
  private List<FileInstance> getFiles() {
    var fileList = new ArrayList<FileInstance>();
    try {
      var resourceUrl = App.class.getResource("/transformationfiles");

      File folder = Paths.get(resourceUrl.toURI()).toFile();

      File[] files = folder.listFiles();
      if (files != null) {
        for (File file : files) {
          if (!file.getName().equals(".DS_Store")) {
            fileList.add(new FileInstance(file.getName(), file.getPath()));
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return fileList;
  }

  /**
   * Handles creating a new Julia set with the specified parameters.
   *
   * @param xmin       the minimum x-coordinate for the drawing area
   * @param ymin       the minimum y-coordinate for the drawing area
   * @param xmax       the maximum x-coordinate for the drawing area
   * @param ymax       the maximum y-coordinate for the drawing area
   * @param transform1 the first transformation matrix
   * @param transform2 the second transformation matrix
   * @param transform3 the third transformation matrix
   * @param steps      the number of steps to run the chaos game
   */
  private void handleCreateNewAffline(double xmin, double ymin, double xmax, double ymax,
      double[] transform1, double[] transform2, double[] transform3, int steps) {
    var description = new ChaosGameDescription(xmin, ymin, xmax, ymax);
    if (transform1.length == 6) {
      description.addTransformation(
          new AffineTransform2D(transform1[0], transform1[1], transform1[2], transform1[3],
              transform1[4], transform1[5]));
    }

    if (transform2.length == 3) {
      description.addTransformation(
          new AffineTransform2D(transform2[0], transform2[1], transform2[2], transform2[3],
              transform2[4], transform2[5]));
    }

    if (transform3.length == 3) {
      description.addTransformation(
          new AffineTransform2D(transform3[0], transform3[1], transform3[2], transform3[3],
              transform3[4], transform3[5]));
    }
    chaosGame.setSteps(steps);
    chaosGame.setNewDescription(description);
  }

  /**
   * Handles creating a new Julia set with the specified parameters.
   *
   * @param xmin      the minimum x-coordinate for the drawing area
   * @param ymin      the minimum y-coordinate for the drawing area
   * @param xmax      the maximum x-coordinate for the drawing area
   * @param ymax      the maximum y-coordinate for the drawing area
   * @param imaginary the imaginary part of the complex number
   * @param real      the real part of the complex number
   * @param steps     the number of steps to run the chaos game
   */
  private void handleCreateNewJulia(double xmin, double ymin, double xmax, double ymax,
      double imaginary, double real, int steps) {
    var description = new ChaosGameDescription(xmin, ymin, xmax, ymax);
    description.addTransformation(new JuliaTransform(new Complex(real, imaginary), 1));
    chaosGame.setSteps(steps);
    chaosGame.setNewDescription(description);
  }
}
