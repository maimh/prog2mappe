package frontend;

import backend.chaosgame.ChaosCanvas;
import backend.chaosgame.ChaosGame;
import backend.chaosgame.ChaosGameDescription;
import backend.filehandler.ChaosGameFileHandler;
import frontend.draw.Drawing;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Pane; // Import Pane class


/**
 * A class for selecting a file and displaying the transformation of a ChaosCanvas on a Pane.
 * Extends the VBox class from JavaFX.
 */
public class FilePicker extends VBox {

  private Button fileButton;
  private ContextMenu fileMenu;
  private Pane drawingArea; // Instance variable to store the drawing area

  /**
   * Constructs a FilePicker with a drawing area.
   *
   * @param drawingArea the Pane to draw on
   */
  public FilePicker(Pane drawingArea) { // Modify constructor to accept drawing area parameter
    this.drawingArea = drawingArea; // Store drawing area reference
    fileButton = new Button("Play Game");
    fileButton.getStyleClass().add("button-style");
    fileMenu = new ContextMenu();
    fileMenu.getStyleClass().add("menu-style");

    fileButton.setOnAction(event -> {
      try {
        populateFiles();
      } catch (URISyntaxException e) {
        throw new RuntimeException(e);
      }
      fileMenu.show(fileButton, Side.BOTTOM, 0, 0);
    });

    drawingArea.setStyle("-fx-border-color: black; -fx-border-width: 1px;");

    this.getChildren().add(fileButton);
  }

  /**
   * Populates the file menu with files from the transformationfiles directory.
   *
   * @throws URISyntaxException if the URI is invalid
   */
  private void populateFiles() throws URISyntaxException {
    fileMenu.getItems().clear();

    var resourceUrl = App.class.getResource("/transformationfiles");
    File folder = Paths.get(resourceUrl.toURI()).toFile();

    File[] files = folder.listFiles();
    if (files != null) {
      for (File file : files) {
        if (!file.getName().equals(".DS_Store")) {
          MenuItem item = new MenuItem(file.getName());
          item.setOnAction(e -> {
            System.out.println("Selected file: " + file.getName());

            ChaosGameDescription fileHandler = null;
            try {
              fileHandler = ChaosGameFileHandler.readFromFile(file.getPath());
            } catch (FileNotFoundException ex) {
              throw new RuntimeException(ex);
            }

            int canvasWidth = 100;
            int canvasHeight = 150;

            ChaosCanvas chaosCanvas = new ChaosCanvas(canvasWidth, canvasHeight);
            ChaosGame game = new ChaosGame(chaosCanvas, fileHandler);

            game.runSteps(10000000);

            drawingArea.getChildren().clear();

            Drawing.drawTransformation(drawingArea, chaosCanvas);
            chaosCanvas.printCanvas();
          });
          fileMenu.getItems().add(item);
        }
      }
    }
  }
}