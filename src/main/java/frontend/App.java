package frontend;


import backend.chaosgame.ChaosGame;
import frontend.controllers.ButtonController;
import frontend.controllers.DrawingController;
import java.util.Objects;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * The main class for the Chaos Game application. Extends the Application class from JavaFX.
 */
public class App extends Application {

  private String currentData = "";

  public static void main(String[] args) {
    launch(args);
  }

  /**
   * Starts the Chaos Game application.
   *
   * @param primaryStage the primary stage for the application
   */
  public void start(Stage primaryStage) {
    ChaosGame game = new ChaosGame();
    var container = new HBox();
    DrawingController drawingController = new DrawingController(game, container);
    ButtonController buttonController = new ButtonController(game, container);
    Scene scene = new Scene(container, 800, 625);
    scene.getStylesheets()
        .add(Objects.requireNonNull(getClass().getResource("/css/round-button")).toExternalForm());
    scene.getStylesheets()
        .add(Objects.requireNonNull(getClass().getResource("/css/clean-button")).toExternalForm());
    scene.getStylesheets()
        .add(Objects.requireNonNull(getClass().getResource("/css/button-style")).toExternalForm());
    scene.getStylesheets()
        .add(Objects.requireNonNull(getClass().getResource("/css/context-meny")).toExternalForm());
    primaryStage.setTitle("Transformation Application");
    primaryStage.setScene(scene);
    primaryStage.show();

  }

}
