package frontend.draw;

import backend.chaosgame.ChaosCanvas;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * A class for drawing the transformation of a ChaosCanvas on a Pane.
 */
public class Drawing {

  /**
   * Draws the transformation of a ChaosCanvas on a Pane.
   *
   * @param pane        the Pane to draw on
   * @param chaosCanvas the ChaosCanvas to draw
   */
  public static void drawTransformation(Pane pane, ChaosCanvas chaosCanvas) {
    int[][] canvasData = chaosCanvas.getCanvas();
    int width = chaosCanvas.getWidth();
    int height = chaosCanvas.getHeight();

    double paneWidth = pane.getWidth();
    double paneHeight = pane.getHeight();

    double cellWidth = paneWidth / width;
    double cellHeight = paneHeight / height;
    // Clear pane
    pane.getChildren().clear();
    // Draw each pixel
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        double x = j * cellWidth;
        double y = i * cellHeight;

        javafx.scene.shape.Rectangle rectangle
            = new javafx.scene.shape.Rectangle(x, y, cellWidth, cellHeight);

        if (canvasData[i][j] == 1) {
          rectangle.setFill(Color.GREEN);
        } else {
          rectangle.setFill(Color.WHITE);
        }
        pane.getChildren().add(rectangle);
      }
    }
  }
}
