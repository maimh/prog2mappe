package frontend.models;

import frontend.Handler;
import java.util.List;

/**
 * Represents a model for a button in the Chaos Game application.
 */
public class ButtonModel {

  /**
   * The event handler for the button.
   */
  public Handler eventHandler;

  /**
   * The list of file instances representing saved Chaos Game descriptions.
   */
  public List<FileInstance> files;
}
