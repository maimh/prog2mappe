package frontend.models;

/**
 * Represents a model for a drawing in the Chaos Game application.
 */
public class DrawingModel {
  /**
   * The 2D array representing the drawing.
   */
  public int[][] data;
  /**
   * The height of the canvas.
   */
  public int canvasHeight;
  /**
   * The width of the canvas.
   */
  public int canvasWidth;

}

