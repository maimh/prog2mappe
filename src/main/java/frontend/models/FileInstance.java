package frontend.models;

/**
 * Represents a file instance in the Chaos Game application.
 */
public class FileInstance {
  public String name;
  public String path;

  /**
   * Constructs a FileInstance with a name and a path.
   *
   * @param name the name of the file
   * @param path the path of the file
   */
  public FileInstance(String name, String path) {
    this.name = name;
    this.path = path;
  }
}

