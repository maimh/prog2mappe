package frontend;

/**
 * Represents a handler for the Chaos Game application.
 */
public interface Handler {

  /**
   * Handles the event of a button being clicked.
   *
   * @param path the path where the file is located
   */
  void handleFile(String path);

  /**
   * Handles the event of clearing the current state.
   */
  void handleClear();

  /**
   * Handles the event of loading a file.
   *
   * @param fileName the name of the file to load
   */
  void handleGetFile(String fileName);

  /**
   * Handles the event of saving the current state to a file.
   *
   * @param fileName the name of the file to save
   */
  void handleSaveFile(String fileName);

  /**
   * Handles the event of creating a new Julia transformation.
   *
   * @param xmin      the minimum x-coordinate
   * @param ymin      the minimum y-coordinate
   * @param xmax      the maximum x-coordinate
   * @param ymax      the maximum y-coordinate
   * @param imaginary the imaginary part of the Julia set constant
   * @param real      the real part of the Julia set constant
   * @param steps     the number of steps to run the transformation
   */
  void handleJuliaData(double xmin, double ymin, double xmax, double ymax, double imaginary,
      double real, int steps);

  /**
   * Handles the event of creating a new affine transformation.
   *
   * @param xmin       the minimum x-coordinate
   * @param ymin       the minimum y-coordinate
   * @param xmax       the maximum x-coordinate
   * @param ymax       the maximum y-coordinate
   * @param transform1 the first transformation matrix
   * @param transform2 the second transformation matrix
   * @param transform3 the third transformation matrix
   * @param steps      the number of steps to run the transformation
   */
  void handleAffineData(double xmin, double ymin, double xmax, double ymax, double[] transform1,
      double[] transform2, double[] transform3, int steps);
}
