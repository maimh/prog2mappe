package frontend.views;

import frontend.models.ButtonModel;
import frontend.models.FileInstance;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Represents a view for a button in the Chaos Game application.
 */
public class ButtonView {

  private ButtonModel model;
  private VBox rightSideButton;
  private VBox underButtons;
  private VBox buttonContainer;
  private VBox filePicker;

  /**
   * Constructs a ButtonView with a container and a model.
   *
   * @param container the HBox container for the button view
   * @param model     the ButtonModel for the view
   */
  public ButtonView(HBox container, ButtonModel model) {
    this.model = model;
    //Buttons
    rightSideButton = new VBox(10);
    rightSideButton.setPadding(new Insets(5));
    filePicker = new VBox(10);
    rightSideButton.setPadding(new Insets(5));

    underButtons = new VBox(10);
    underButtons.setPadding(new Insets(5));
    buttonContainer = new VBox(rightSideButton, filePicker, underButtons);
    container.getChildren().add(buttonContainer);
    drawUnderButtons();
    drawRightSideButtons();
    drawFileMenu();
    draw(model);
  }

  /**
   * Draws the button view with the given model.
   *
   * @param model the ButtonModel to draw
   */
  public void draw(ButtonModel model) {
    this.model = model;
  }

  /**
   * Draws the file menu with the files in the model.
   */
  public void drawFileMenu() {
    filePicker.getChildren().clear();
    Button fileButton = new Button("Play Game");
    fileButton.getStyleClass().add("button-style");
    ContextMenu fileMenu = new ContextMenu();

    fileMenu.getStyleClass().add("menu-style");
    filePicker.getChildren().add(fileButton);
    for (FileInstance file : model.files) {
      MenuItem item = new MenuItem(file.name);
      item.setOnAction(e -> {
        model.eventHandler.handleGetFile(file.path);
      });
      fileMenu.getItems().add(item);
    }

    fileButton.setOnAction(e -> {
      fileMenu.show(fileButton, Side.BOTTOM, 0, 0); // Position the menu right below the button
    });

  }

  /**
   * Draws the right side buttons for creating affine and Julia transformations.
   */
  public void drawRightSideButtons() {
    Button affineButton = new Button("Create Affine");
    affineButton.getStyleClass().add("button-style");
    affineButton.setOnAction(event -> showCreateAffinePopup());

    Button juliaButton = new Button("Create Julia");
    juliaButton.getStyleClass().add("button-style");
    juliaButton.setOnAction(event -> showCreateJuliaPopup());

    Button fileButton = new Button("Play game");
    fileButton.getStyleClass().add("button-style");

    Button save = new Button();
    save.getStyleClass().add("round-button");
    save.setOnAction(actionEvent -> showSaveFileDialog());

    Button clean = new Button();
    clean.getStyleClass().add("clean-button");

    this.rightSideButton.getChildren().clear();
    this.rightSideButton.getChildren().addAll(affineButton, juliaButton);

  }

  /**
   * Draws the under buttons for saving and clearing the canvas.
   */
  public void drawUnderButtons() {
    Button save = new Button();
    save.getStyleClass().add("round-button");
    save.setOnAction(actionEvent -> showSaveFileDialog());

    Button clean = new Button();
    clean.getStyleClass().add("clean-button");
    clean.setOnAction(actionEvent -> model.eventHandler.handleClear());

    this.underButtons.getChildren().clear();
    this.underButtons.getChildren().addAll(save, clean);
  }

  /**
   * Shows a save file dialog for the user to save the current fractal to a file.
   */
  private void showSaveFileDialog() {
    try {
      Stage saveDialog = new Stage();
      saveDialog.initModality(Modality.APPLICATION_MODAL);
      saveDialog.setTitle("Save your creation");

      Label label = new Label("Pathname:");
      TextField pathnameField = new TextField();
      Button saveButton = new Button("Save");
      Button cancelButton = new Button("Cancel");

      saveButton.setOnAction(e -> {
        if (!pathnameField.getText().isEmpty()) {
          model.eventHandler.handleFile(pathnameField.getText());
          saveDialog.close();
        } else {
          Alert alert = new Alert(Alert.AlertType.ERROR, "Please enter a filename.");
          alert.showAndWait();
        }
      });

      cancelButton.setOnAction(e -> saveDialog.close());

      VBox layout = new VBox(10);
      layout.getChildren().addAll(label, pathnameField, saveButton, cancelButton);
      layout.setPadding(new Insets(10));
      layout.setAlignment(Pos.CENTER);

      Scene scene = new Scene(layout, 300, 150);
      saveDialog.setScene(scene);
      saveDialog.showAndWait();
    } catch (Exception e) {
      e.printStackTrace();
      Alert alert = new Alert(Alert.AlertType.ERROR,
          "Error showing save file dialog: " + e.getMessage());
      alert.showAndWait();
    }
  }

  /**
   * Shows a popup for the user to create a Julia fractal. Parts of the method is AI generated with
   * CoPilot
   */
  private void showCreateJuliaPopup() {
    Stage window = new Stage();
    window.initModality(Modality.APPLICATION_MODAL);
    window.setTitle("Create Julia");

    GridPane grid = new GridPane();
    grid.setPadding(new Insets(10));
    grid.setVgap(8);
    grid.setHgap(10);
    grid.setStyle(
        "-fx-background-color: #f191cc;");

    Label labelSteps = new Label("Steps:");
    GridPane.setConstraints(labelSteps, 0, 0);
    TextField inputSteps = new TextField();
    GridPane.setConstraints(inputSteps, 1, 0, 2, 1);

    // Input fields for vector v1
    Label labelV1 = new Label("v1:");
    GridPane.setConstraints(labelV1, 0, 1);
    TextField inputV1X = new TextField();
    inputV1X.setPromptText("X");
    GridPane.setConstraints(inputV1X, 1, 1);
    TextField inputV1Y = new TextField();
    inputV1Y.setPromptText("Y");
    GridPane.setConstraints(inputV1Y, 2, 1);

    // Input fields for vector v2
    Label labelV2 = new Label("v2:");
    GridPane.setConstraints(labelV2, 0, 2);
    TextField inputV2X = new TextField();
    inputV2X.setPromptText("X");
    GridPane.setConstraints(inputV2X, 1, 2);
    TextField inputV2Y = new TextField();
    inputV2Y.setPromptText("Y");
    GridPane.setConstraints(inputV2Y, 2, 2);

    // Rest of the labels and text fields...
    Label im = new Label("Imaginary:");
    GridPane.setConstraints(im, 0, 3);
    TextField imaginary = new TextField();
    imaginary.setPromptText("Imaginary part");
    GridPane.setConstraints(imaginary, 1, 3);
    Label re = new Label("Real:");
    GridPane.setConstraints(re, 0, 4);
    TextField real = new TextField();
    GridPane.setConstraints(real, 1, 4);
    real.setPromptText("Real part");

    HBox colorBox = new HBox(5);
    Label colorLabel = new Label("Color:");
    ColorPicker colorPicker = new ColorPicker();
    colorBox.setPadding(new Insets(0, 0, 0, 10));
    colorBox.getChildren().addAll(colorPicker);

    Button createAndDrawButton = new Button("Create");
    GridPane.setConstraints(createAndDrawButton, 2, 5);
    createAndDrawButton.setOnAction(e -> {
      try {
        var inputV1XValue = Double.parseDouble(inputV1X.getText());
        var inputV1YValue = Double.parseDouble(inputV1Y.getText());
        var inputV2XValue = Double.parseDouble(inputV2X.getText());
        var inputV2YValue = Double.parseDouble(inputV2Y.getText());
        var imaginaryValue = Double.parseDouble(imaginary.getText());
        var realValue = Double.parseDouble(real.getText());

        var stepsValue = Integer.parseInt(inputSteps.getText());
        model.eventHandler.handleJuliaData(inputV1XValue, inputV1YValue, inputV2XValue,
            inputV2YValue, imaginaryValue, realValue, stepsValue);
        window.close();
      } catch (Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Please enter valid numbers.");
        alert.showAndWait();
      }


    });

    // Adding all the components to the grid
    grid.getChildren().addAll(
        labelSteps, inputSteps,
        labelV1, inputV1X, inputV1Y,
        labelV2, inputV2X, inputV2Y, im, imaginary, re, real,
        //colorBox,
        createAndDrawButton
    );

    Scene scene = new Scene(grid, 500, 300);
    window.setScene(scene);
    window.showAndWait();
  }


  /**
   * Shows a popup for the user to create an affine fractal. Parts of the method is AI generated
   * with CoPilot
   */
  private void showCreateAffinePopup() {
    Stage popupStage = new Stage();
    popupStage.initModality(Modality.APPLICATION_MODAL);
    popupStage.setTitle("Create Affine");

    VBox layout = new VBox(10);
    layout.setAlignment(Pos.CENTER);
    layout.setStyle("-fx-background-color: #f191cc; -fx-padding: 20;");

    // Steps input
    HBox stepsBox = new HBox(5);
    Label stepsLabel = new Label("Steps:");
    TextField stepsField = new TextField();
    stepsField.setPrefColumnCount(5);
    stepsBox.getChildren().addAll(stepsLabel, stepsField);

    // Matrix and Vector input
    GridPane inputGrid = new GridPane();
    inputGrid.setHgap(10);
    inputGrid.setVgap(10);
    TextField[][] matrixFields = new TextField[3][6]; //første fra 3 til 4
    TextField[][] vectorFields = new TextField[2][2];

    for (int i = 0; i < 3; i++) { // For 3 transformations
      Label matrixLabel = new Label("Transformation " + (i + 1));
      GridPane matrixGrid = new GridPane();
      matrixGrid.setHgap(5);
      matrixGrid.setVgap(5);
      for (int j = 0; j < 6; j++) {
        TextField field = new TextField();
        field.setPrefWidth(50);
        matrixFields[i][j] = field;
        matrixGrid.add(field, j % 3, j / 3); //
      }
      inputGrid.add(matrixLabel, i, 0);
      inputGrid.add(matrixGrid, i, 1);
    }

    for (int i = 0; i < 2; i++) { // For 2 vectors
      VBox vectorBox = new VBox(5);
      Label vectorLabel = new Label("v" + (i + 1));
      for (int j = 0; j < 2; j++) {
        TextField field = new TextField();
        field.setPrefWidth(50);
        vectorFields[i][j] = field;
        vectorBox.getChildren().add(field);
      }
      inputGrid.add(vectorLabel, 3 + i, 0);
      inputGrid.add(vectorBox, 3 + i, 1);
    }

    // Color picker
    HBox colorBox = new HBox(5);
    Label colorLabel = new Label("Color:");
    ColorPicker colorPicker = new ColorPicker();
    colorBox.getChildren().addAll(colorLabel, colorPicker);

    //
    // Create button
    Button createButton = new Button("Create");
    createButton.setOnAction(e -> {
      try {
        var transform1 = new double[]{
            Double.parseDouble(matrixFields[0][0].getText()),
            Double.parseDouble(matrixFields[0][1].getText()),
            Double.parseDouble(matrixFields[0][2].getText()),
            Double.parseDouble(matrixFields[0][3].getText()),
            Double.parseDouble(matrixFields[0][4].getText()),
            Double.parseDouble(matrixFields[0][5].getText()),
        };
        var transform2 = new double[]{
            Double.parseDouble(matrixFields[1][0].getText()),
            Double.parseDouble(matrixFields[1][1].getText()),
            Double.parseDouble(matrixFields[1][2].getText()),
            Double.parseDouble(matrixFields[1][3].getText()),
            Double.parseDouble(matrixFields[1][4].getText()),
            Double.parseDouble(matrixFields[1][5].getText()),
        };
        var transform3 = new double[]{
            Double.parseDouble(matrixFields[2][0].getText()),
            Double.parseDouble(matrixFields[2][1].getText()),
            Double.parseDouble(matrixFields[2][2].getText()),
            Double.parseDouble(matrixFields[2][3].getText()),
            Double.parseDouble(matrixFields[2][4].getText()),
            Double.parseDouble(matrixFields[2][5].getText()),
        };
        var xmin = Double.parseDouble(vectorFields[0][0].getText());
        var ymin = Double.parseDouble(vectorFields[0][1].getText());
        var xmax = Double.parseDouble(vectorFields[1][0].getText());
        var ymax = Double.parseDouble(vectorFields[1][1].getText());
        var steps = Integer.parseInt(stepsField.getText());
        model.eventHandler.handleAffineData(xmin, ymin, xmax, ymax, transform1, transform2,
            transform3, steps);
        popupStage.close();
      } catch (Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Please enter valid numbers.");
        alert.showAndWait();
      }

    });

    // Add all components to layout
    layout.getChildren().addAll(stepsBox, inputGrid, createButton);

    Scene scene = new Scene(layout);
    popupStage.setScene(scene);
    popupStage.showAndWait();
  }

}
