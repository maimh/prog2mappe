package frontend.views;

import frontend.models.DrawingModel;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * A class for drawing the transformation of a ChaosCanvas on a Pane.
 */
public class DrawingView {

  private DrawingModel model;
  private Pane drawingArea;

  /**
   * Constructs a DrawingView with a DrawingModel and a container for the view.
   *
   * @param model     the DrawingModel to draw
   * @param container the HBox container for the view
   */
  public DrawingView(DrawingModel model, HBox container) {
    this.model = model;
    drawingArea = new Pane();
    drawingArea.setStyle(
        "-fx-background-color: white; -fx-border-color: black; -fx-border-width: 1px;");
    drawingArea.setPrefSize(600, 500); // Set preferred size
    container.getChildren().add(drawingArea);
  }

  /**
   * Draws the transformation of a ChaosCanvas on a Pane.
   *
   * @param model the DrawingModel to draw
   */
  public void draw(DrawingModel model) {
    this.model = model;
    drawArea();
  }

  /**
   * Draws the transformation of a ChaosCanvas on a Pane.
   */
  public void drawArea() {
    drawingArea.getChildren().clear();
    if (model.data != null) {

      int[][] canvasData = model.data;
      int width = model.canvasWidth;
      int height = model.canvasHeight;

      double paneWidth = drawingArea.getWidth();
      double paneHeight = drawingArea.getHeight();

      double cellWidth = paneWidth / width;
      double cellHeight = paneHeight / height;
      // Draw each pixel
      for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
          double x = j * cellWidth;
          double y = i * cellHeight;

          javafx.scene.shape.Rectangle rectangle
              = new javafx.scene.shape.Rectangle(x, y, cellWidth, cellHeight);

          if (canvasData[i][j] == 1) {
            rectangle.setFill(Color.GREEN);
          } else {
            rectangle.setFill(Color.WHITE);
          }
          drawingArea.getChildren().add(rectangle);
        }
      }
    }
  }
}
