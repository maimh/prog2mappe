package backend.filehandler;

import backend.chaosgame.ChaosGameDescription;
import backend.math.Complex;
import backend.transformations.AffineTransform2D;
import backend.transformations.JuliaTransform;
import backend.transformations.Transform2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Handles reading and writing ChaosGameDescription objects to and from files.
 */
public class ChaosGameFileHandler {

  /**
   * Reads a ChaosGameDescription object from a file.
   *
   * @param filePath the path to the file to read from
   * @return the ChaosGameDescription object read from the file
   * @throws FileNotFoundException if the file is not found
   */

  public static ChaosGameDescription readFromFile(String filePath) throws FileNotFoundException {
    File file = new File(filePath);
    Scanner scanner = new Scanner(file);
    ChaosGameDescription description = new ChaosGameDescription(0, 0, 1, 1);

    if (scanner.hasNextLine()) {
      String type = scanner.nextLine().trim();

      if ("Affine2D".equals(type)) {
        readAffineDescription(scanner, description);
      } else if ("Julia".equals(type)) {
        readJuliaDescription(scanner, description);
      }
    }

    scanner.close();
    return description;
  }


  /**
   * Reads a ChaosGameDescription object from a file.
   *
   * @param filePath    the path to the file to read from
   * @param description the ChaosGameDescription object to read into
   * @throws FileNotFoundException if the file is not found
   */
  public static void readFromFile(String filePath, ChaosGameDescription description)
      throws FileNotFoundException {
    File file = new File(filePath);
    Scanner scanner = new Scanner(file);
    if (scanner.hasNextLine()) {
      String type = scanner.nextLine().trim();

      if ("Affine2D".equals(type)) {
        readAffineDescription(scanner, description);
      } else if ("Julia".equals(type)) {
        readJuliaDescription(scanner, description);
      }
    }

    scanner.close();
  }

  /**
   * Reads an affine transformation description from a scanner and adds the transformations to the
   * given ChaosGameDescription object.
   *
   * @param scanner     the scanner to read from
   * @param description the ChaosGameDescription object to add the transformations to
   */
  private static void readAffineDescription(Scanner scanner, ChaosGameDescription description) {
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      double[] params = parseDoubles(line);
      if (params.length == 6) {
        //AffineTransform2D constructor takes (a00, a01, a10, a11, b0, b1)
        description.addTransformation(
            new AffineTransform2D(params[0], params[1], params[2], params[3], params[4],
                params[5]));
      }
    }
  }

  /**
   * Reads a Julia transformation description from a scanner and adds the transformations to the
   * given ChaosGameDescription object.
   *
   * @param scanner     the scanner to read from
   * @param description the ChaosGameDescription object to add the transformations to
   */
  private static void readJuliaDescription(Scanner scanner, ChaosGameDescription description) {
    scanner.nextLine();
    scanner.nextLine();

    if (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      double[] constant = parseDoubles(line);
      if (constant.length == 2) {
        description.addTransformation(new JuliaTransform(new Complex(constant[0], constant[1]), 1));
      }
    }
  }

  /**
   * Saves a ChaosGameDescription object to a file with the specified filename.
   *
   * @param filename    the name of the file to save to
   * @param description the ChaosGameDescription object to save
   */
  public static void saveDescriptionToFile(String filename, ChaosGameDescription description) {
    try {
      StringBuilder sb = new StringBuilder();
      var lines = new ArrayList<String>();
      if (!description.getTransformations().isEmpty()) {

        if (description.isAffine()) {
          lines.add("Affine2D");
          sb.append(description.getX0min()).append(",").append(description.getX1min());
          lines.add(sb.toString());
          sb.setLength(0);
          sb.append(description.getX0max()).append(",").append(description.getX1max());
          lines.add(sb.toString());
          sb.setLength(0);
          for (Transform2D transform : description.getTransformations()) {
            var values = transform.getValues();
            sb.append(values[0]).append(",").append(values[1]).append(",").append(values[2]).append(",")
                .append(values[3]).append(",").append(values[4]).append(",").append(values[5]);
            lines.add(sb.toString());
            sb.setLength(0);
          }
        } else {
          lines.add("Julia");
          sb.append(description.getX0min()).append(",").append(description.getX1min());
          lines.add(sb.toString());
          sb.setLength(0);
          sb.append(description.getX0max()).append(",").append(description.getX1max());
          lines.add(sb.toString());
          sb.setLength(0);
          for (Transform2D transform : description.getTransformations()) {
            var values = transform.getValues();
            sb.append(values[0]).append(",").append(values[1]);
            lines.add(sb.toString());
            sb.setLength(0);
          }
        }
        if (lines.size() > 0) {
          String basePath = System.getProperty("user.dir") + "/" + filename;
          try (PrintWriter myWriter = new PrintWriter(basePath, "UTF-8")) {
            for (String line : lines) {
              myWriter.println(line);
            }
          }

        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   * Parses a line of comma-separated doubles into an array of doubles.
   *
   * @param line the line to parse
   * @return an array of doubles parsed from the line
   */
  private static double[] parseDoubles(String line) {
    String[] parts = line.split(",\\s*");
    double[] nums = new double[parts.length];
    for (int i = 0; i < parts.length; i++) {
      nums[i] = Double.parseDouble(parts[i]);
    }
    return nums;
  }
}