package backend.transformations;

import backend.math.Vector2D;

/**
 * Represents a 2D transformation that applies a specific mathematical operation to a given 2D
 * vector (point).
 */

public interface Transform2D {

  /**
   * Transforms a given 2D vector (point) by applying the transformation.
   *
   * @param point the 2D vector to be transformed
   * @return the transformed 2D vector
   */
  Vector2D transform(Vector2D point);

  /**
   * Returns the values of the transformation.
   *
   * @return the values of the transformation
   */
  double[] getValues();

}

