package backend.transformations;

import backend.math.Complex;
import backend.math.Vector2D;

/**
 * Represents a Julia transformation in 2D space. This transformation applies a specific
 * mathematical operation to a given 2D vector (point). Implements the Transform2D interface.
 */
public class JuliaTransform implements Transform2D {

  private Complex point;
  private int sign;

  /**
   * Constructs a JuliaTransform with a specified complex point and sign.
   *
   * @param point the complex point used in the transformation
   * @param sign  the sign (+1 or -1) used to determine the direction of the transformation
   */
  public JuliaTransform(Complex point, int sign) {
    this.point = point;
    this.sign = sign;
  }

  /**
   * Transforms a given 2D vector (point) by applying the Julia transformation.
   *
   * @param point the 2D vector to be transformed
   * @return the transformed 2D vector
   */
  public Vector2D transform(Vector2D point) {
    Complex newPoint = new Complex(point.getX0(), point.getX1());
    return newPoint.sqrt();
  }


  /**
   * Returns the values of the complex point used in the transformation.
   *
   * @return the values of the complex point
   */
  @Override
  public double[] getValues() {
    return new double[]{
        this.point.getX0(),
        this.point.getX1()

    };
  }


}
