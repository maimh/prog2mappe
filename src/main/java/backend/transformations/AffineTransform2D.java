package backend.transformations;

import backend.math.Matrix2x2;
import backend.math.Vector2D;

/**
 * Represents a 2D affine transformation. This transformation involves applying a linear
 * transformation followed by a translation. Implements the Transform2D interface.
 */
public class AffineTransform2D implements Transform2D {

  private final Matrix2x2 matrix;
  private final Vector2D vector;

  /**
   * Constructs an AffineTransform2D with a specified matrix and vector.
   *
   * @param a00 the element at row 0, column 0 of the transformation matrix
   * @param a01 the element at row 0, column 1 of the transformation matrix
   * @param a10 the element at row 1, column 0 of the transformation matrix
   * @param a11 the element at row 1, column 1 of the transformation matrix
   * @param b0  the x-component of the translation vector
   * @param b1  the y-component of the translation vector
   */
  public AffineTransform2D(double a00, double a01, double a10, double a11, double b0, double b1) {
    this.matrix = new Matrix2x2(a00, a01, a10, a11);
    this.vector = new Vector2D(b0, b1);
  }

  /**
   * Transforms a given 2D vector (point) by applying the affine transformation. This involves
   * multiplying the point by the transformation matrix and then adding the translation vector.
   *
   * @param point the point to be transformed
   * @return the transformed point as a new Vector2D
   */
  @Override
  public Vector2D transform(Vector2D point) {
    return matrix.multiplyVectorAndMatrix(point).add(vector);
  }


  @Override
  public double[] getValues() {
    var values = new double[6];
    int i = 0;
    for (double d : matrix.getValues()) {
      values[i] = d;
      i++;
    }
    values[4] = vector.getX0();
    values[5] = vector.getX1();
    return values;
  }


}
