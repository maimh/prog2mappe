package backend.interfaces;

/**
 * An interface for classes that observe the ChaosGame class.
 * Implement the update method to receive updates from the ChaosGame class.
 */
public interface ChaosGameObserver {
  /**
   * Updates the observer with new information from the ChaosGame class.
   * Implement this method to define the behavior that should occur when an update happens.
   */
  void update();
}
