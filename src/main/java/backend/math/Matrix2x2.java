package backend.math;

/**
 * Represents a 2x2 matrix with four components, a00, a01, a10, and a11.
 */
public class Matrix2x2 {

  private double a00;
  private double a01;
  private double a10;
  private double a11;

  /**
   * Constructs a 2x2 matrix with four components, a00, a01, a10, and a11.
   *
   * @param a00 the element at row 0, column 0 of the matrix
   * @param a01 the element at row 0, column 1 of the matrix
   * @param a10 the element at row 1, column 0 of the matrix
   * @param a11 the element at row 1, column 1 of the matrix
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Multiplies this matrix by another matrix.
   *
   * @param vector the vector to be multiplied by the matrix
   * @return a new Vector2D that is the result of the multiplication
   */
  public Vector2D multiplyVectorAndMatrix(Vector2D vector) {
    double newX0 = this.a00 * vector.getX0() + this.a01 * vector.getX1();
    double newX1 = this.a10 * vector.getX0() + this.a11 * vector.getX1();
    return new Vector2D(newX0, newX1);
  }

  /**
   * Returns the values of the matrix as an array.
   *
   * @return an array containing the values of the matrix
   */
  public double[] getValues() {
    return new double[]{
        a00,
        a01,
        a10,
        a11
    };
  }

}