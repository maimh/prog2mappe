package backend.math;


/**
 * Represents a complex number in 2D space. This class extends the Vector2D class and adds methods
 * for complex number operations.
 */
public class Complex extends Vector2D {

  /**
   * Constructs a complex number with a real and imaginary component.
   *
   * @param real      the real component of the complex number
   * @param imaginary the imaginary component of the complex number
   */
  public Complex(double real, double imaginary) {
    super(real, imaginary);
  }

  /**
   * Returns the square root of the complex number.
   *
   * @return a new Complex number that is the square root of the original complex number
   */
  public Complex sqrt() {
    double r = Math.sqrt(this.getX0() * this.getX0() + this.getX1() * this.getX1());
    double newReal = Math.sqrt((r + this.getX0()) / 2);
    double newImaginary = Math.signum(this.getX1()) * Math.sqrt((r - this.getX0()) / 2);
    return new Complex(newReal, newImaginary);
  }

  /**
   * Returns the real component of the complex number.
   *
   * @return the real component of the complex number
   */
  public double getReal() {
    return this.getX0();
  }

  /**
   * Returns the imaginary component of the complex number.
   *
   * @return the imaginary component of the complex number
   */
  public double getImaginary() {
    return this.getX1();
  }

}
