package backend.math;

/**
 * Represents a 2D vector with two components, x0 and x1.
 */
public class Vector2D {

  private double x0;
  private double x1;

  /**
   * Constructs a 2D vector with two components, x0 and x1.
   *
   * @param x0 the first component of the vector
   * @param x1 the second component of the vector
   */

  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Returns the first component of the vector, x0.
   *
   * @return the first component of the vector
   */
  public double getX0() {
    return x0;
  }

  /**
   * Returns the second component of the vector, x1.
   *
   * @return the second component of the vector
   */
  public double getX1() {
    return x1;
  }

  /**
   * Adds this vector to another vector.
   *
   * @param other the other vector to be added
   * @return the sum of the two vectors as a new Vector2D
   */
  public Vector2D add(Vector2D other) {
    double newX0 = this.x0 + other.getX0();
    double newX1 = this.x1 + other.getX1();
    return new Vector2D(newX0, newX1);
  }

  /**
   * Subtracts another vector from this vector.
   *
   * @param other the other vector to be subtracted
   * @return the difference of the two vectors as a new Vector2D
   */
  public Vector2D subtract(Vector2D other) {
    double newX0 = this.x0 - other.getX0();
    double newX1 = this.x1 - other.getX1();
    return new Vector2D(newX0, newX1);

  }

}

