package backend.chaosgame;

import backend.filehandler.ChaosGameFileHandler;
import backend.transformations.Transform2D;
import java.util.ArrayList;
import java.util.List;


/**
 * Represents a description of a chaos game. Contains a list of transformations and the coordinates
 * for the drawing area.
 */
public class ChaosGameDescription {

  private List<Transform2D> transformations;
  private double x0min;
  private double x1min;
  private double x0max;
  private double x1max;



  /**
   * Constructs a ChaosGameDescription with default coordinates for the drawing area.
   */
  public ChaosGameDescription() {
    this(0.0, 0.0, 1.0, 1.0);
  }

  /**
   * Constructs a ChaosGameDescription with specified coordinates for the drawing area.
   *
   * @param x0min the minimum x-coordinate for the drawing area
   * @param x1min the minimum y-coordinate for the drawing area
   * @param x0max the maximum x-coordinate for the drawing area
   * @param x1max the maximum y-coordinate for the drawing area
   */
  public ChaosGameDescription(double x0min, double x1min, double x0max, double x1max) {
    this.x0min = x0min;
    this.x1min = x1min;
    this.x0max = x0max;
    this.x1max = x1max;
    this.transformations = new ArrayList<>();
  }

  /**
   * Adds a transformation to the list of transformations.
   *
   * @param transform the transformation to add
   */
  public void addTransformation(Transform2D transform) {
    transformations.add(transform);
  }

  public List<Transform2D> getTransformations() {
    return transformations;
  }

  /**
   * Returns the minimum x-coordinate for the drawing area.
   *
   * @return the minimum x-coordinate for the drawing area
   */
  public double getX0min() {
    return x0min;
  }

  /**
   * Returns the minimum y-coordinate for the drawing area.
   *
   * @return the minimum y-coordinate for the drawing area
   */
  public double getX1min() {
    return x1min;
  }

  /**
   * Returns the maximum x-coordinate for the drawing area.
   *
   * @return the maximum x-coordinate for the drawing area
   */
  public double getX0max() {
    return x0max;
  }

  /**
   * Returns the maximum y-coordinate for the drawing area.
   *
   * @return the maximum y-coordinate for the drawing area
   */
  public double getX1max() {
    return x1max;
  }

  /**
   * Saves the ChaosGameDescription to a file with the specified path.
   *
   * @param path the path to save the description to
   */
  public void saveToFile(String path) {
    ChaosGameFileHandler.saveDescriptionToFile(path, this);
  }

  /**
   * Reads a ChaosGameDescription from a file with the specified path.
   *
   * @param path the path to read the description from
   */
  public void fromFile(String path) {
    try {
      ChaosGameFileHandler.readFromFile(path, this);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Returns true if all transformations in the list are affine transformations.
   *
   * @return true if all transformations are affine, false otherwise
   */
  public boolean isAffine() {
    for (Transform2D transform : transformations) {
      if (!(transform instanceof backend.transformations.AffineTransform2D)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Clears the list of transformations.
   */
  public void clearTransformations() {
    transformations.clear();
  }

  public void setTransformations(List<Transform2D> transformations) {
    this.transformations = transformations;
  }
}

