package backend.chaosgame;

import backend.interfaces.ChaosGameObserver;
import backend.math.Vector2D;
import backend.transformations.Transform2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Represents a chaos game. Contains a canvas for plotting points and a description of the game.
 */

public class ChaosGame {

  private final ChaosCanvas canvas;
  private List<ChaosGameObserver> observers;
  private ChaosGameDescription description;
  private int steps;

  private final Random random = new Random();
  private Vector2D currentPoint;

  /**
   * Constructs a ChaosGame with a default canvas and description.
   */
  public ChaosGame() {
    this.canvas = new ChaosCanvas();
    this.description = new ChaosGameDescription();
    this.currentPoint = new Vector2D(0.5, 0.5);
    this.observers = new ArrayList<>();
    this.steps = 1000000; ///Riktig steps 1000000;
  }

  /**
   * Constructs a ChaosGame with a specified canvas and description.
   *
   * @param canvas      the canvas to use
   * @param description the description to use
   */
  public ChaosGame(ChaosCanvas canvas, ChaosGameDescription description) {
    this.canvas = canvas;
    this.description = description;
    this.currentPoint = new Vector2D(0.5, 0.5);
    this.observers = new ArrayList<>();
    this.steps = 1000000;
  }

  /**
   * Runs the chaos game for a specified number of steps.
   *
   * @param steps the number of steps to run
   */
  public void runSteps(int steps) {
    List<Transform2D> transformations = description.getTransformations();
    if (transformations == null || transformations.isEmpty()) {
      throw new IllegalArgumentException("No transformations in description");
    }

    for (int i = 0; i < steps; i++) {
      int randomIndex;
      randomIndex = random.nextInt(description.getTransformations().size());
      Transform2D selectedTransform = description.getTransformations().get(randomIndex);

      currentPoint = selectedTransform.transform(currentPoint);

      canvas.plotPoint(currentPoint);
    }
  }

  /**
   * Clears the canvas and resets the current point.
   */
  public void clear() {
    this.canvas.clearCanvas();
    this.description.clearTransformations();
    this.currentPoint = new Vector2D(0.5, 0.5);
  }

  /**
   * Sets a new description for the chaos game and runs the game for the specified number of steps.
   *
   * @param description the new description
   */
  public void setNewDescription(ChaosGameDescription description) {
    this.clear();
    this.description = description;
    this.runSteps(this.steps);
    notifyObservers();
  }


  /**
   * Reads a description from a file and runs the chaos game for the specified number of steps.
   *
   * @param fileName the name of the file to read from
   */
  public void readDescriptionFromFile(String fileName) {
    this.clear();
    this.description.fromFile(fileName);
    this.runSteps(this.steps);
    this.notifyObservers();
  }

  /**
   * Returns the canvas of the chaos game.
   *
   * @return the canvas data
   */
  public int[][] getCanvas() {
    return this.canvas.getCanvas();
  }

  /**
   * Sets the number of steps to run the chaos game for.
   *
   * @param steps the number of steps to run
   */
  public void setSteps(int steps) {
    this.steps = steps;
  }

  /**
   * Clears the canvas.
   */
  public void clearCanvas() {
    this.canvas.clearCanvas();
  }

  /**
   * Returns the height of the canvas.
   *
   * @return the height of the canvas
   */
  public int getCanvasHeight() {
    return this.canvas.getHeight();
  }

  /**
   * Returns the width of the canvas.
   *
   * @return the width of the canvas
   */
  public int getCanvasWidth() {
    return this.canvas.getWidth();
  }

  /**
   * Saves the description of the chaos game to a file.
   *
   * @param path the path to save the description to
   */
  public void saveToFile(String path) {
    description.saveToFile(path);
    notifyObservers();
  }

  /**
   * Trigger the notify method for all observers.
   */
  public void triggerNotify() {
    notifyObservers();
  }

  /**
   * Registers an observer to the chaos game.
   *
   * @param observer the observer to register
   */
  public void regigsterObserver(ChaosGameObserver observer) {
    this.observers.add(observer);
  }

  /**
   * Notifies all observers of the chaos game.
   */
  private void notifyObservers() {
    for (ChaosGameObserver observer : this.observers) {
      observer.update();
    }
  }
}

