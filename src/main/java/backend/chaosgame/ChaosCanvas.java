package backend.chaosgame;

import backend.math.Vector2D;

/**
 * Represents a canvas for plotting points in a chaos game.
 */

public class ChaosCanvas {

  private final int[][] canvas;
  private final int width;
  private final int height;


  /**
   * Returns the canvas as a 2D array of integers.
   *
   * @return the canvas data
   */
  public int[][] getCanvas() {
    return canvas;
  }

  /**
   * Returns the width of the canvas.
   *
   * @return the canvas width
   */
  public int getWidth() {
    return width;
  }

  /**
   * Returns the height of the canvas.
   *
   * @return the canvas height
   */
  public int getHeight() {
    return height;
  }

  /**
   * Constructs a ChaosCanvas with default dimensions.
   */
  public ChaosCanvas() {
    this(500, 800);
  }

  /**
   * Constructs a ChaosCanvas with specified dimensions.
   *
   * @param width  the width of the canvas
   * @param height the height of the canvas
   */
  public ChaosCanvas(int width, int height) {
    this.width = width;
    this.height = height;
    this.canvas = new int[height][width];
  }

  /**
   * Plots a point on the canvas. The point coordinares are assumed to be normalized [0, 1].
   *
   * @param point the point to plot
   */
  public void plotPoint(Vector2D point) {
    // Mapping continuous to discrete, assuming x and y are normalized [0, 1]
    int x = (int) ((point.getX0() + 2.65) / 5.3 * this.width);
    int y = (int) ((1.0 - (point.getX1() / 10.0))
        * this.height);

    if (x >= 0 && x < width && y >= 0 && y < height) {
      canvas[y][x] = 1;
    }
  }

  /**
   * Clears the canvas by setting all points to 0.
   */
  public void clearCanvas() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        canvas[i][j] = 0;
      }
    }
  }

  /**
   * Prints the canvas to the console.
   */
  public void printCanvas() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        System.out.print(canvas[i][j] == 1 ? "X" : " ");
      }
      System.out.println();
    }
  }

}