package backend.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Matrix2x2Test {

  Matrix2x2 matrix;
  Vector2D vector;


  @BeforeEach
  void setUp() {
    matrix = new Matrix2x2(1, 2, 3, 4);
    vector = new Vector2D(1, 2);
  }

  @Test
  void multiplyVectorAndMatrix() {
    Vector2D result = matrix.multiplyVectorAndMatrix(vector);
    assertEquals(5, result.getX0());
    assertEquals(11, result.getX1());
  }

  @Test
  void multiplyVectorByScalarMatrix() {
    double scalar = 2;
    Matrix2x2 scalarMatrix = new Matrix2x2(scalar, 0, 0, scalar);
    Vector2D vector = new Vector2D(2, 3);
    Vector2D result = scalarMatrix.multiplyVectorAndMatrix(vector);
    assertEquals(4, result.getX0());
    assertEquals(6, result.getX1());
  }

  @Test
  void multiplyVectorByMatrixWithNegativeElements() {
    Matrix2x2 matrixWithNegatives = new Matrix2x2(-1, -2, -3, -4);
    Vector2D vector = new Vector2D(2, 3);
    Vector2D result = matrixWithNegatives.multiplyVectorAndMatrix(vector);
    assertEquals(-8, result.getX0());
    assertEquals(-18, result.getX1());
  }

}
