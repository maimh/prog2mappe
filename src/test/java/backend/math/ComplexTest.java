package backend.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ComplexTest {

  Complex complex;


  @BeforeEach
  void setUp() {
    complex = new Complex(3, 4);
  }

  @Test
  void sqrt() {
    Complex result = complex.sqrt();
    assertEquals(2, result.getX0());
    assertEquals(1, result.getX1());
  }

  //*
  // * Checks the validity of the real part of the complex number. It ensures that the
  @Test
  void newReal() {
    assertEquals(3, complex.getX0());
    assertNotEquals(4, complex.getX0());
  }

  @Test
  void getImaginary() {
    assertEquals(4, complex.getX1());
    assertNotEquals(3, complex.getX1());
  }
}
