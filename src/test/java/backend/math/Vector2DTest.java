package backend.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Vector2DTest {
  Vector2D vector;
  Vector2D vector2;

  @BeforeEach
  void setUp() {
    vector = new Vector2D(1, 2);
    vector2 = new Vector2D(3, 4);
  }

  @Test
  void getX0andX1() {
    assertEquals(1, vector.getX0());
    assertEquals(2, vector.getX1());
  }

  @Test
  void testNegativeX0andX1() {
    assertNotEquals(2, vector.getX0());
    assertNotEquals(1, vector.getX1());
  }

  @Test
  void addVectorToAnotherVector() {
    Vector2D result = vector.add(vector2);
    assertEquals(4, result.getX0());
    assertEquals(6, result.getX1());
  }

  @Test
  void testAddVectorToItself() {
    Vector2D result = vector.add(vector);
    assertEquals(2, result.getX0());
    assertEquals(4, result.getX1());

  }

  @Test
  void subtract() {
    Vector2D result = vector.subtract(vector2);
    assertEquals(-2, result.getX0());
    assertEquals(-2, result.getX1());
  }


}
