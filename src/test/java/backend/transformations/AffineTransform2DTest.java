package backend.transformations;

import static org.junit.jupiter.api.Assertions.*;

import backend.math.Matrix2x2;
import backend.math.Vector2D;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AffineTransform2DTest {

  Matrix2x2 matrix;
  Vector2D vector;
  AffineTransform2D affineTransform2D;

  @BeforeEach
  void setUp() {
    matrix = new Matrix2x2(1,2,3,4);
    vector = new Vector2D(1,2);
    affineTransform2D = new AffineTransform2D(1,2,3,4,1,2);
  }

  @AfterEach
  void tearDown() {
    AffineTransform2D affineTransform2D = null;
  }

  /**
   * Tests the transformation of a point using the affine transformation matrix.
   * The point is positive.
   */
  @Test
  void transformTest() {
    Vector2D point = new Vector2D(1, 2);
    Vector2D result = affineTransform2D.transform(point);
    assertEquals(6, result.getX0());
    assertEquals(13, result.getX1());
  }

}
