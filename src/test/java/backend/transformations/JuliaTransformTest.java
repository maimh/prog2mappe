package backend.transformations;

import static org.junit.jupiter.api.Assertions.*;

import backend.math.Complex;
import backend.math.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class JuliaTransformTest {
  JuliaTransform juliaTransform;
  Vector2D inputVector;
  Complex inputPoint;

  @BeforeEach
  void setUp() {
  }

  @Test
  void transformPositive() {
    juliaTransform = new JuliaTransform(new Complex(1, 2), 1);
    inputPoint = new Complex(3, 4);
    inputVector = juliaTransform.transform(inputPoint);
    assertEquals(2, inputVector.getX0(), 0.0001);
    assertEquals(1, inputVector.getX1(), 0.0001);
  }

  @Test
  void transformNegative() {
    juliaTransform = new JuliaTransform(new Complex(-1, -2), -1);
    inputPoint = new Complex(-3, -4);
    inputVector = juliaTransform.transform(inputPoint);
    assertEquals(1, inputVector.getX0(), 0.0001);
    assertEquals(-2, inputVector.getX1(), 0.0001);
  }
}
