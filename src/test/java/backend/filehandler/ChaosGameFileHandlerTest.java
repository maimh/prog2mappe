package backend.filehandler;

import static org.junit.jupiter.api.Assertions.*;

import backend.chaosgame.ChaosGameDescription;
import backend.math.Complex;
import backend.transformations.AffineTransform2D;
import backend.transformations.JuliaTransform;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ChaosGameFileHandlerTest {

  private final String TEST_DIR = "src/tests/resources/";
  private final String TEST_FILE_AFFINE = TEST_DIR + "test_affine.txt";
  private final String TEST_FILE_JULIA = TEST_DIR + "test_julia.txt";
  private final String OUTPUT_FILE = TEST_DIR + "output.txt";


  @BeforeEach
  void setUp() throws Exception {
    new File(TEST_DIR).mkdirs();

    try (PrintWriter writer = new PrintWriter(TEST_FILE_AFFINE)) {
      writer.println("Affine2D");
      writer.println("0.5, 0.0, 0.0, 0.5, 0.0, 0.0");
      writer.println("0.5, 0.0, 0.0, 0.5, 0.5, 0.0");
      writer.println("0.5, 0.0, 0.0, 0.5, 0.25, 0.5");
    }

    try (PrintWriter writer = new PrintWriter(TEST_FILE_JULIA)) {
      writer.println("Julia");
      writer.println("0.5, 0.5");
      writer.println("0.25, 0.25");
      writer.println("0.75, 0.75");

    }
  }

  @AfterEach
  public void tearDown() throws Exception {
    new File(TEST_FILE_AFFINE).delete();
    new File(TEST_FILE_JULIA).delete();
    new File(OUTPUT_FILE).delete();
  }


  @Test
  void testIfIdentifyRightTransformationTypePositive() throws Exception {
    ChaosGameDescription description = ChaosGameFileHandler.readFromFile(TEST_FILE_AFFINE);
    assertTrue(description.isAffine());
  }

  @Test
  void testIfIdentifyRightTransformationTypeNegative() throws Exception {
    ChaosGameDescription description = ChaosGameFileHandler.readFromFile(TEST_FILE_JULIA);
    assertFalse(description.isAffine());
  }

  @Test
  void testIfReadAffineDescriptionPositive() throws Exception {
    ChaosGameDescription description = ChaosGameFileHandler.readFromFile(TEST_FILE_AFFINE);
    assertEquals(3, description.getTransformations().size());
  }

  @Test
  void testIfReadJuliaDescriptionPositive() throws Exception {
    ChaosGameDescription description = ChaosGameFileHandler.readFromFile(TEST_FILE_JULIA);
    assertEquals(1, description.getTransformations().size(), "Expected 1 Julia transformation");
  }

  @Test
  void testIfHandleInvalidFile() {
    assertThrows(FileNotFoundException.class, () -> {
      ChaosGameFileHandler.readFromFile(TEST_DIR + "non_existent_file.txt");
    }, "Expected FileNotFoundException for non-existent file");
  }

  @Test
  void testSaveDescriptionToFileAffine() throws Exception {
    ChaosGameDescription description = new ChaosGameDescription(0.0, 0.0, 1.0, 1.0);
    description.addTransformation(new AffineTransform2D(1.0, 0.0, 0.0, 1.0, 0.0, 0.0));

    ChaosGameFileHandler.saveDescriptionToFile( TEST_DIR + "output.txt", description);

    File file = new File(OUTPUT_FILE);
    assertTrue(file.exists(), "Expected output file to be created");

    try (Scanner scanner = new Scanner(file)) {
      assertEquals("Affine2D", scanner.nextLine().trim());
      assertEquals("0.0,0.0", scanner.nextLine().trim());
      assertEquals("1.0,1.0", scanner.nextLine().trim());
      assertEquals("1.0,0.0,0.0,1.0,0.0,0.0", scanner.nextLine().trim());
    }
  }

  @Test
  void testSaveDescriptionToFileJulia() throws Exception {
    ChaosGameDescription description = new ChaosGameDescription(0.0, 0.0, 1.0, 1.0);
    description.addTransformation(new JuliaTransform(new Complex(0.5, 0.5), 1));

    ChaosGameFileHandler.saveDescriptionToFile( TEST_DIR + "output.txt", description);

    File file = new File(OUTPUT_FILE);
    assertTrue(file.exists(), "Expected output file to be created");

    try (Scanner scanner = new Scanner(file)) {
      assertEquals("Julia", scanner.nextLine().trim());
      assertEquals("0.0,0.0", scanner.nextLine().trim());
      assertEquals("1.0,1.0", scanner.nextLine().trim());
      assertEquals("0.5,0.5", scanner.nextLine().trim());
    }
  }


  @Test
  void testReadFromFileWithDescriptionForAffine() throws Exception {
    ChaosGameDescription description = new ChaosGameDescription(0, 0, 1, 1);
    ChaosGameFileHandler.readFromFile(TEST_FILE_AFFINE, description);
    assertTrue(description.isAffine(), "Expected true when all transformations are affine");
    assertEquals(3, description.getTransformations().size(), "Expected 3 affine transformations");
  }

  @Test
  void testReadFromFileWithDescriptionForJulia() throws Exception {
    ChaosGameDescription description = new ChaosGameDescription(0, 0, 1, 1);
    ChaosGameFileHandler.readFromFile(TEST_FILE_JULIA, description);
    assertFalse(description.isAffine(), "Expected false when there are non-affine transformations");
    assertEquals(1, description.getTransformations().size(), "Expected 1 Julia transformation");
  }



}
