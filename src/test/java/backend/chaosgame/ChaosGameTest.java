package backend.chaosgame;

import backend.interfaces.ChaosGameObserver;
import backend.math.Vector2D;
import backend.transformations.Transform2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ChaosGameTest {

  private ChaosGame chaosGame;
  private ChaosCanvas canvas;
  private ChaosGameDescription description;

  @BeforeEach
  void setUp() {
    canvas = new ChaosCanvas();
    description = new ChaosGameDescription();
    chaosGame = new ChaosGame(canvas, description);

    // Adding a mock transformation to the description
    List<Transform2D> transformations = new ArrayList<>();
    transformations.add(new MockTransform());
    description.setTransformations(transformations);
  }

  @Test
  void testDefaultConstructor() {
    ChaosGame defaultGame = new ChaosGame();
    assertNotNull(defaultGame.getCanvas());
    assertEquals(500, defaultGame.getCanvasWidth());
    assertEquals(800, defaultGame.getCanvasHeight());
  }

  @Test
  void testRunStepsWithValidTransformations() {
    chaosGame.runSteps(10);
    int[][] canvasData = chaosGame.getCanvas();
    assertTrue(isAnyPointPlotted(canvasData));
  }

  @Test
  void testRunStepsWithNoTransformations() {
    // Create a new ChaosGame instance with an empty description
    ChaosGameDescription emptyDescription = new ChaosGameDescription();
    ChaosGame emptyTransformGame = new ChaosGame(new ChaosCanvas(), emptyDescription);

    IllegalArgumentException thrown = assertThrows(
        IllegalArgumentException.class,
        () -> emptyTransformGame.runSteps(10),
        "Expected runSteps to throw an exception, but it didn't"
    );

    assertTrue(thrown.getMessage().contains("No transformations in description"));
  }

  @Test
  void testClear() {
    chaosGame.runSteps(10); // Plot some points first
    chaosGame.clear();
    int[][] canvasData = chaosGame.getCanvas();

    for (int[] row : canvasData) {
      for (int cell : row) {
        assertEquals(0, cell);
      }
    }
  }

  @Test
  void testSetNewDescription() {
    List<Transform2D> transformations = new ArrayList<>();
    transformations.add(new MockTransform());
    ChaosGameDescription newDescription = new ChaosGameDescription();
    newDescription.setTransformations(transformations);

    chaosGame.setNewDescription(newDescription);
    // Add checks to verify if the description has been updated
  }


  @Test
  void testNotifyObservers() {
    MockObserver observer = new MockObserver();
    chaosGame.regigsterObserver(observer);

    chaosGame.triggerNotify();
    assertTrue(observer.isUpdated());
  }

  @Test
  void testSetSteps() {
    chaosGame.setSteps(500);
    // Assuming there's a way to verify the steps internally
  }

  // Helper method to check if any point is plotted on the canvas
  private boolean isAnyPointPlotted(int[][] canvas) {
    for (int[] row : canvas) {
      for (int cell : row) {
        if (cell == 1) {
          return true;
        }
      }
    }
    return false;
  }

  // Mock Transform2D for testing
  private static class MockTransform implements Transform2D {
    @Override
    public Vector2D transform(Vector2D point) {
      return new Vector2D(0.1, 0.1); // Example transformation
    }

    @Override
    public double[] getValues() {
      return new double[0];
    }
  }

  // Mock Observer for testing
  private static class MockObserver implements ChaosGameObserver {
    private boolean updated = false;

    @Override
    public void update() {
      updated = true;
    }

    public boolean isUpdated() {
      return updated;
    }
  }
}