package backend.chaosgame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import backend.math.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ChaosCanvasTest {
  private ChaosCanvas canvas;

  /*
    * Set up a new ChaosCanvas object before each test.
   */
  @BeforeEach
  void setUp() {
    canvas = new ChaosCanvas();
  }

  /*
    * Test the constructor of the ChaosCanvas class.
   */
  @Test
  void testConstructor() {
    assertEquals(500, canvas.getWidth());
    assertEquals(800, canvas.getHeight());
  }

  /*
    * Test the parameterized constructor of the ChaosCanvas class.
   */
  @Test
  void testParameterizedConstructor() {
    ChaosCanvas customCanvas = new ChaosCanvas(300, 600);
    assertEquals(300, customCanvas.getWidth());
    assertEquals(600, customCanvas.getHeight());
  }

  /*
    * Test plotting a point within the bounds of the canvas.
    * The point is plotted at (1.0, 1.0) which should be within the bounds of the canvas.
   */
  @Test
  void testPlotPointWithinBoundsPositiv() {
    Vector2D point = new Vector2D(1.0, 1.0);
    canvas.plotPoint(point);
    int x = (int)((point.getX0() + 2.65) / 5.3 * canvas.getWidth());
    int y = (int)((1.0 - (point.getX1() / 10.0)) * canvas.getHeight());
    assertEquals(1, canvas.getCanvas()[y][x]);
  }

  /*
    * Test plotting a point out of the bounds of the canvas.
    * The point is plotted at (10.0, 10.0) which should be out of the bounds of the canvas.
   */
  @Test
  void testPlotPointOutOfBounds() {
    Vector2D point = new Vector2D(10.0, 10.0);
    canvas.plotPoint(point);
    // Expect no changes to the canvas
    for (int i = 0; i < canvas.getHeight(); i++) {
      for (int j = 0; j < canvas.getWidth(); j++) {
        assertEquals(0, canvas.getCanvas()[i][j]);
      }
    }
  }

  /*
  *Test clearing the canvas.
  * Plot a point on the canvas and then clear it.
  * The canvas should be empty after clearing.
   */
  @Test
  void testClearCanvas() {
    Vector2D point = new Vector2D(0.5, 0.5);
    canvas.plotPoint(point);
    canvas.clearCanvas();
    for (int i = 0; i < canvas.getHeight(); i++) {
      for (int j = 0; j < canvas.getWidth(); j++) {
        assertEquals(0, canvas.getCanvas()[i][j]);
      }
    }
  }

}